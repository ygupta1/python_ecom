// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.5.0;
import "./openzeppelin-contracts/contracts/token/ERC20/ERC20.sol";
import "./openzeppelin-contracts/contracts/token/ERC20/ERC20Burnable.sol";
import "./openzeppelin-contracts/contracts/token/ERC20/ERC20Pausable.sol";
import "./openzeppelin-contracts/contracts/token/ERC20/ERC20Mintable.sol";
import "./interface/Helper.sol";

contract YToken is ERC20, ERC20Burnable, ERC20Pausable, ERC20Mintable
{
    string public name;
    string public symbol;
    address private helper;
    
    constructor(string memory name_, string memory symbol_, address helper_) public {
        name = name_;
        symbol = symbol_;
        helper = helper_;
    }
    
    function sum(address a, address b) public view returns(uint256) {
        uint256 balance_a = balanceOf(a);
        uint256 balance_b = balanceOf(b);
        Helper h = Helper(helper);
        uint256 result = h.add(balance_a, balance_b);
        return result;
    }
}






