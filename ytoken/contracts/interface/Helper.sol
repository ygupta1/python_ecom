// SPDX-License-Identifier: GPL-3.0

pragma solidity ^0.5.0;

interface Helper {
    function add(uint256 first, uint256 second) external pure returns(uint256);
}