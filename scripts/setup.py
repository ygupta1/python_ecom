import argparse
import os
import json
from pathlib import Path
from solcx import compile_files, get_solc_version, set_solc_version
from tqdm import tqdm
import time

def read_config():
    config_file = Path(__file__).parent / ".." / '.ytoken' / 'config.json'
    with open(config_file, 'r') as f:
        config = json.load(f)
    return config

def connect_to_network(network, account='user'):
    if network == 'local':
        from web3 import Web3

        w3 = Web3(Web3.HTTPProvider("http://127.0.0.1:7545"))
        try:
            w3.eth.defaultAccount = w3.eth.accounts[0]
            admin = w3.eth.accounts[0]
        except:
            raise Exception("Ensure ganache-cli is connected")

    elif network == 'ropsten':
        config = read_config()
        os.environ['WEB3_INFURA_PROJECT_ID'] = config['infura']['project_id']
        os.environ['WEB3_INFURA_API_SECRET'] = config['infura']['secret']

        from web3.middleware import construct_sign_and_send_raw_middleware
        from web3.auto.infura.ropsten import w3

        admin = w3.eth.account.from_key(config[account]['key'])
        w3.eth.defaultAccount = admin.address
        w3.middleware_onion.add(construct_sign_and_send_raw_middleware(admin))

    else:
        raise ValueError(f'Unknown network {network}')

    assert w3.isConnected()
    return w3, admin

def compile_contract(contract_file, build_file):
    set_solc_version('0.5.1')
    compiled_sol = compile_files(contract_file)
    contract_id, contract_interface = compiled_sol.popitem()
    with open(build_file, "w") as outfile:
        json.dump(contract_interface, outfile)

    print("Contract compiled succesfully - ", contract_file)

def compile():
    for i in tqdm (range (100), desc="Compiling...", ascii=False, ncols=75):
        time.sleep(0.01)

    ytoken_contract_file = Path(__file__).parent / ".." / 'ytoken' / 'contracts' / 'ytoken.sol'
    ytoken_build_file = Path(__file__).parent / ".." / 'ytoken' / 'build' / 'contracts' / 'ytoken.json'
    
    help_contract_file = Path(__file__).parent / ".." / 'ytoken' / 'contracts' / 'help.sol'
    help_build_file = Path(__file__).parent / ".." / 'ytoken' / 'build' / 'contracts' / 'help.json'
    
    if not os.path.isfile(ytoken_contract_file):
        print('No Ytoken Contract File')
        return
    
    if not os.path.isfile(help_contract_file):
        print('No help Contract File')
        return

    compile_contract(help_contract_file, help_build_file)
    compile_contract(ytoken_contract_file, ytoken_build_file)

def deploy_contract(w3, contract, *args):
    tx_hash = contract.constructor( *args).transact()
    tx_receipt = w3.eth.waitForTransactionReceipt(tx_hash)
    deployed_contract = w3.eth.contract(
        address=tx_receipt.contractAddress,
        abi=contract.abi
    )
    return deployed_contract

def deploy(w3, contracts, cache_file_name='contract_address.json'):
    for i in tqdm (range (100), desc="Deploying...", ascii=False, ncols=75):
        time.sleep(0.01)

    cache_file = Path(__file__).parent / 'contract-cache' / cache_file_name
    help = deploy_contract(w3, contracts['Help'])
    ytoken = deploy_contract(w3, contracts['Ytoken'], "Yash", "YT", help.address)

    contract_addresses = {
        'Ytoken': ytoken.address,
        'Help': help.address,
    }
    with open(cache_file, 'w') as f:
        json.dump(contract_addresses, f)

    deployed_contracts = {
        'Ytoken': ytoken,
        'Help': help,
    }

    print("\nContract deployed Successfully at address - ", contract_addresses)
    return deployed_contracts

def connect_contract(w3, contract, address):
    deployed_contract = w3.eth.contract(
        address=address,
        abi=contract.abi
    )
    return deployed_contract

def connect(w3, contracts, cache_file_name='contract_address.json'):
    for i in tqdm (range (100), desc="Connecting...", ascii=False, ncols=75):
        time.sleep(0.01)
    cache_file = Path(__file__).parent / 'contract-cache' / cache_file_name

    if not os.path.isfile(cache_file):
        print('No address file available first deploy')
        return

    with open(cache_file, 'r') as f:
        contract_cache = json.load(f)
    deployed_contracts = {}
    for k in contracts.keys():
        if contract_cache[k]:
            deployed_contracts[k] = connect_contract(w3, contracts[k], contract_cache[k])
        else:
            print('\nNo address available for ', k)
        print("\nContract connected Successfully at address - ", deployed_contracts[k].address)
    print('\n')
    return deployed_contracts
    
def create_contract(w3, build_file):
    with open(build_file, 'r') as f:
        contract_details = json.load(f)
    abi = contract_details['abi']
    bytecode = contract_details['bin']
    contract = w3.eth.contract(abi=abi, bytecode=bytecode)
    return contract

def get_contracts(w3):
    ytoken_build_file = Path(__file__).parent / ".." / 'ytoken' / 'build' / 'contracts' / 'ytoken.json'
    help_build_file = Path(__file__).parent / ".." / 'ytoken' / 'build' / 'contracts' / 'help.json'
        
    if not os.path.isfile(ytoken_build_file):
        print('No Ytoken Build File Please Compile First')
        return
    
    if not os.path.isfile(help_build_file):
        print('No Help Build File Please Compile First')
        return

    contracts = {
        'Ytoken': create_contract(w3, ytoken_build_file),
        'Help': create_contract(w3, help_build_file)
    }
    return contracts

if __name__ == '__main__':
    parser = argparse.ArgumentParser('Ecommerce Contract Setup')
    parser.add_argument(
        '--action', '-a', dest='action', default='connect',
        help='Action to perform: compile, connect, deploy, setup(default)'
    )
    parser.add_argument(
        '--network', '-n', dest='network', default='local',
        help='For connecting to local or ropsten network'
    )

    args = parser.parse_args()
    assert args.network in {'local', 'ropsten'}

    w3, admin = connect_to_network(args.network, 'admin')

    if args.action == 'compile':
        compile()
    elif args.action == 'deploy':
        contracts = get_contracts(w3)
        if not contracts is None:
            deployed_contracts = deploy(w3, contracts)
    elif args.action == 'connect':
        contracts = get_contracts(w3)
        if not contracts is None:
            deployed_contracts = connect(w3, contracts)
    elif args.action == 'setup':
        compile()
        contracts = get_contracts(w3)
        deployed_contracts = deploy(w3, contracts)
    else:
        raise ValueError(f'Unknown action: {args.action}')

    ytoken = deployed_contracts['Ytoken'];

    # print("My token Name is - ", ytoken.functions.name().call())
    # print("My token Symbol is - ", ytoken.functions.symbol().call())
    # print("My token Total Supply is - ", ytoken.functions.totalSupply().call())

    # print("\n")
    # ytoken.functions.mint(w3.eth.accounts[0], 1000).transact()
    # print("Successfully minted amount 1000 to the address", w3.eth.accounts[0])
    # print("Now Balance of account ", w3.eth.accounts[0], "is", ytoken.functions.balanceOf(w3.eth.accounts[0]).call())

    # print("\n")
    # ytoken.functions.mint(w3.eth.accounts[1], 1000).transact()
    # print("Successfully minted amount 1000 to the address", w3.eth.accounts[1])
    # print("Now Balance of account ", w3.eth.accounts[1], "is", ytoken.functions.balanceOf(w3.eth.accounts[1]).call())
    
    # print("\n")
    # print("My token Total Supply is - ", ytoken.functions.totalSupply().call())
    # print("Addition of balance of this two account", w3.eth.accounts[0], w3.eth.accounts[1], "is", ytoken.functions.sum(w3.eth.accounts[0], w3.eth.accounts[1]).call())



    
    